﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class EnemyAIBase : Entity
{
    [HideInInspector]
    protected Actions m_actions;

    private Stats m_stats;

    [SerializeField]
    private Slider healthBar;

    private Color m_barStartColor;

    private Camera playerCam;

    private NavMeshAgent m_agent;

    [SerializeField]
    private float m_detectDistance;

    [SerializeField]
    private float m_attackDistance;

    [HideInInspector]
    protected float m_attackTimer;

    [SerializeField]
    protected float m_attackMaxTimer;

    [SerializeField]
    private LayerMask m_mask;

    protected override void Awake()
    {
        base.Awake();
        m_stats = GetComponent<Stats>();
        playerCam = FindObjectOfType<Camera>();
        base.entityType = EntityType.Enemy;
        m_barStartColor = healthBar.colors.normalColor;
        m_agent = GetComponent<NavMeshAgent>();
        m_actions = GetComponent<Actions>();
    }

    protected override void Update()
    {
        base.Update();
        UpdateHealthBar();
        DoLogic();
    }

    private void UpdateHealthBar()
    {
        healthBar.value = m_stats.health / m_stats.maxHealth;
        if (playerCam == null)
            playerCam = FindObjectOfType<Camera>();
        healthBar.transform.LookAt(2 * transform.position - playerCam.transform.position);
    }

    public void FlashHealthBar()
    {
        Image im = healthBar.transform.GetChild(1).GetChild(0).GetComponent<Image>();
        StartCoroutine(FlashHealthBar(im, 0.1f));
    }

    private IEnumerator FlashHealthBar(Image im, float dur)
    {
        im.color = Color.white;
        yield return new WaitForSeconds(dur);
        im.color = m_barStartColor;
    }

    //maybe needs to be virtual?
    protected virtual void DoLogic()
    {
        //fast dirty test change this
        Player p = FindObjectOfType<Player>();
       
        RaycastHit hit;
        Physics.Raycast(transform.position, (p.transform.position - transform.position).normalized, out hit, m_detectDistance, m_mask);
       
        if (hit.collider != null && hit.collider.transform.root.GetComponent<Player>() != null)
        {
            //Debug.Log(hit.collider);
            
            m_agent.destination = p.transform.position - m_agent.transform.forward * 1.5f;

            if (Vector3.Distance(m_agent.transform.position, m_agent.destination) < 1.5f)
                Attack();
            m_attackTimer -= Time.deltaTime;
            if (m_attackTimer <= 0)
                m_attackTimer = 0;
        }
    }

    protected virtual void Attack()
    {
        if(m_attackTimer <= 0)
        {
            m_actions.DoAction(m_actions.CurrentActions[ActionType.MainAttack1]);
            m_attackTimer = m_attackMaxTimer;
        }
    }
}

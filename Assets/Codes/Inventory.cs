﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Inventory : MonoBehaviour
{
    // Inventory
    public List<ItemBase> inv = new List<ItemBase>();

    // Equiped items
    public Weapon rhWeapon;
    public Weapon lhWeapon;
    public Armour torso;

    private void Awake()
    {

    }

    public void EquipRhWeapon(Entity e, Weapon w)
    {
        Weapon newW = w;
        e.inventory.inv.Remove(w); // Remove new weapon from inventory
        if (e.inventory.rhWeapon != null)
        {
            for(int i=0;i< e.stats.Damages.Count; i++)
            { // Find the correct damage from entity
                if (e.stats.Damages[i].damageTypes == e.inventory.rhWeapon.baseDamage.damageTypes && e.stats.Damages[i].amount == e.inventory.rhWeapon.baseDamage.amount)
                {
                    float temp = e.stats.health;

                    e.stats.Damages.Remove(e.stats.Damages[i]); // Remove old weapon from entity's stats

                    if (e.stats.health < temp) // Health won't be affected by item change
                        e.stats.health = temp;

                    break;
                }
            }
            e.inventory.inv.Add(e.inventory.rhWeapon); // Move old weapon to inventory
        }

        
        e.stats.Damages.Add(newW.baseDamage); // Add new weapon to entity's stats

        e.inventory.rhWeapon = newW;
    }
    public void EquipLhWeapon(Entity e, Weapon w)
    {
        Weapon newW = w;
        e.inventory.inv.Remove(w); // Remove new weapon from inventory
        if (e.inventory.lhWeapon != null)
        {
            for (int i = 0; i < e.stats.Damages.Count; i++)
            { // Find the correct damage from entity
                if (e.stats.Damages[i].damageTypes == e.inventory.lhWeapon.baseDamage.damageTypes && e.stats.Damages[i].amount == e.inventory.lhWeapon.baseDamage.amount)
                {
                    float temp = e.stats.health;

                    e.stats.Damages.Remove(e.stats.Damages[i]); // Remove old weapon from entity's stats

                    if (e.stats.health < temp) // Health won't be affected by item change
                        e.stats.health = temp;

                    break;
                }
            }
            e.inventory.inv.Add(e.inventory.lhWeapon); // Move old weapon to inventory
        }


        e.stats.Damages.Add(newW.baseDamage); // Add new weapon to entity's stats

        e.inventory.lhWeapon = newW;
    }
    public void EquipTorsoArmour(Entity e, Armour a)
    {
        Armour newA = a;
        e.inventory.inv.Remove(a); // Remove new armour from inventory
        if (e.inventory.torso != null)
        {
            for (int i = 0; i < e.stats.Resistances.Count; i++)
            { // Find the correct resistance from entity
                if (e.stats.Resistances[i].damageTypes == e.inventory.torso.baseResistance.damageTypes && e.stats.Resistances[i].amount == e.inventory.torso.baseResistance.amount)
                {
                    float temp = e.stats.health;

                    e.stats.Damages.Remove(e.stats.Resistances[i]); // Remove old armour from entity's stats

                    if (e.stats.health < temp) // Health won't be affected by item change
                        e.stats.health = temp;

                    break;
                }
            }
            e.inventory.inv.Add(e.inventory.torso); // Move old armour to inventory
        }


        e.stats.Damages.Add(newA.baseResistance); // Add new armour to entity's stats

        e.inventory.torso = newA;
    }

}

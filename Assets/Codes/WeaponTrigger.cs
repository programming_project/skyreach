﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponTrigger : MonoBehaviour
{
    public bool dealingDmg;
    public List<DamageTypes> dTS_ChangeThis;
    public float weaponDmg_ChangeThis;

    private Entity owner;

    private void Awake()
    {
        owner = transform.root.GetComponent<Entity>();
    }

    private List<Stats> hitList = new List<Stats>();

    private void OnTriggerStay(Collider col)
    {
        Stats s = col.GetComponent<Stats>();
        if (s != null && dealingDmg && s.transform.root.GetComponent<Entity>() != owner && !hitList.Contains(s))
        {
            s.TakeDamage(dTS_ChangeThis, weaponDmg_ChangeThis);
            hitList.Add(s);
        }
    }

    public void DoHit()
    {
        dealingDmg = true;
    }

    public void ClearHitList()
    {
        dealingDmg = false;
        hitList.Clear();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Entity
{
    private Actions m_actions;
    private Stats m_stats;

    //target locking
    private List<Entity> m_closeTargets = new List<Entity>();
    private Entity m_currentTarget;
    private bool m_locked;

    [SerializeField]
    private Animator m_anim;

    public Animator Anim { get { return m_anim; } set { m_anim = value; } }

    [SerializeField]
    private Canvas m_playerUI;

    public Canvas PlayerUI { get { return m_playerUI; } set { m_playerUI = value; } }

    [SerializeField]
    private Slider m_healthBar;

    public Slider HealthBar { get { return m_healthBar; } set { m_healthBar = value; } }

    [SerializeField]
    private Slider m_staminaBar;

    public Slider StaminaBar { get { return m_staminaBar; } set { m_staminaBar = value; } }

    public bool Locked
    {
        get
        {
            return m_locked;
        }
    }

    public Entity CurrentTarget
    {
        get
        {
            return m_currentTarget;
        }
        
    }

    public void AddTarget(Entity e)
    {
        m_closeTargets.Add(e);
    }

    public void RemoveTarget(Entity e)
    {
        if(m_closeTargets.Contains(e))
            m_closeTargets.Remove(e);
    }

    protected override void Awake()
    {
        base.Awake();
        m_actions = GetComponent<Actions>();
        m_stats = GetComponent<Stats>();
    }

    protected override void Update()
    {
        base.Update();
        if (Input.GetKeyDown(KeyCode.F))
        {
            m_locked = true;
            SetCurrentTarget();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            m_locked = false;
            m_currentTarget = null;
        }

        //if our traget drops and we have other targetable entities close, target next
        if (m_locked && m_currentTarget == null && m_closeTargets.Count > 0)
            SetCurrentTarget();

        //TODO: Implement keybind system here
        if (Input.GetKeyDown(KeyCode.R))
            m_actions.DoAction(m_actions.CurrentActions[ActionType.Roll]);

        if (Input.GetKeyDown(KeyCode.Mouse0))
            m_actions.DoAction(m_actions.CurrentActions[ActionType.MainAttack1]);

        if (Input.GetKeyDown(KeyCode.Mouse1))
            m_actions.DoAction(m_actions.CurrentActions[ActionType.MainAttack2]);

        UpdateUI();
    }

    private void SetCurrentTarget()
    {
        int index = 0;
        if(m_currentTarget != null)
            index = m_closeTargets.IndexOf(m_currentTarget);

        if(m_closeTargets.Count > 0)
        {
            if (index < m_closeTargets.Count - 1)
                m_currentTarget = m_closeTargets[index + 1];
            else
                m_currentTarget = m_closeTargets[0];
        }
    }

    private void UpdateUI()
    {
        m_healthBar.value = m_stats.health / m_stats.maxHealth;
        m_staminaBar.value = m_stats.stamina / m_stats.maxStamina;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invisibility : Buff
{
    private Stats m_stats;
    private Entity m_owner;

    public Invisibility(float duration, Entity bOwner)
    {
        base.m_buffDuration = duration;
        m_owner = bOwner;
        m_stats = bOwner.GetComponent<Stats>();
    }

    public override void DoUpdate()
    {
        base.DoUpdate();
    }

    public override void StartBuffTimer()
    {
        base.StartBuffTimer();
        m_stats.canTakeDmg = false;
        m_owner.GetComponent<BuffSystem>().buffList.Add(this);
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        m_stats.canTakeDmg = true;
        m_owner.GetComponent<BuffSystem>().buffList.Remove(this);
    }
}
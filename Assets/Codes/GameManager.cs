﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    //Used for singleton
    public static GameManager GM;

    //Create Keycodes that will be associated with each of our commands.
    //These can be accessed by any other script in our game
    public KeyCode jump { get; set; }
    public KeyCode forward { get; set; }
    public KeyCode backward { get; set; }
    public KeyCode left { get; set; }
    public KeyCode right { get; set; }

    public Transform playerSpawner;

    [SerializeField]
    private Player m_playerPrefab;
    [SerializeField]
    private CameraController m_playerCamPrefab;
    [SerializeField]
    private Canvas m_playerUIPrefab;

    //our player object in game
    [HideInInspector]
    public Player player;
    [HideInInspector]
    public CameraController playerCam;
    [HideInInspector]
    public Canvas playerUI;

    public ItemDatabase iDatabase;

    void Awake()
    {
        GM = this;
        //Singleton pattern
        /*if (GM == null)
        {
            DontDestroyOnLoad(gameObject);
            GM = this;
        }
        else if (GM != this)
        {
            Destroy(gameObject);
        }*/
        /*Assign each keycode when the game starts.
         * Loads data from PlayerPrefs so if a user quits the game,
         * their bindings are loaded next time. Default values
         * are assigned to each Keycode via the second parameter
         * of the GetString() function
         */
        jump = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("jumpKey", "Space"));
        forward = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("forwardKey", "W"));
        backward = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("backwardKey", "S"));
        left = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("leftKey", "A"));
        right = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("rightKey", "D"));

        //Initialize and instantiate player, UI and camera
        player = Instantiate(m_playerPrefab, playerSpawner.position, Quaternion.identity);
        playerCam = Instantiate(m_playerCamPrefab, playerSpawner.position, Quaternion.identity);
        playerUI = Instantiate(m_playerUIPrefab, playerSpawner.position, Quaternion.identity);

        player.PlayerUI = playerUI;
        Slider[] sliders = player.PlayerUI.GetComponentsInChildren<Slider>();
        player.HealthBar = sliders[0];
        player.StaminaBar = sliders[1];

        player.GetComponent<Movement>().PlayerCam = playerCam.GetComponent<Camera>();
        playerCam.playerTransform = player.transform;
        playerCam.Rig = playerCam.playerTransform.GetComponent<Rigidbody>();
        playerCam.Player = playerCam.playerTransform.GetComponent<Player>();
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillTrigger : MonoBehaviour
{
    public Transform checkPoint;
    void OnTriggerEnter(Collider col)
    {
        if (col.GetComponent<Player>())
            col.transform.position = checkPoint.position;
        //Destroy(col.gameObject);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Roll
/// </summary>
[Serializable]
public class Roll : Action
{
    public Roll(ActionType aType, int requiredStamina)
    {
        base.aType = aType;
        base.requiredStamina = requiredStamina;
    }

    public override void DoAction(Entity owner)
    {
        Invisibility invB = new Invisibility(0.2f, owner);
        invB.StartBuffTimer();

        float rollDistance = 250;
        Rigidbody rBody = owner.GetComponent<Rigidbody>();
        if (rBody == null)
        {
            Debug.LogError("NO RIGID BODY AT " + owner.Name + "!");
            return;
        }

        //add invisibility frames
        //disable movement for small amount of time to perfrom roll
        if (owner.GetComponent<Movement>())
        {
            if(owner.GetComponent<Player>().CurrentTarget == null)
            {
                owner.GetComponent<Movement>().AllowMovement = false;
                rBody.AddForce(owner.transform.forward * rollDistance);
            }
            else
            {
                //this is not wroking properly if you press a and w for exaple it rolls to right angle, but too fast
                owner.GetComponent<Movement>().AllowMovement = false;
                if(Input.GetAxis("Horizontal") > 0)
                    rBody.AddForce(rBody.transform.right * rollDistance);
                if (Input.GetAxis("Horizontal") < 0)
                    rBody.AddForce(-rBody.transform.right * rollDistance);
                if (Input.GetAxis("Vertical") > 0 || Input.GetAxis("Vertical") == 0 && Input.GetAxis("Horizontal") == 0)
                    rBody.AddForce(rBody.transform.forward * rollDistance);
                if (Input.GetAxis("Vertical") < 0)
                    rBody.AddForce(-rBody.transform.forward * rollDistance);
            }

            owner.GetComponent<Player>().Anim.SetTrigger("Roll");
        }

        /*Vector3 r = Movement.GetVectorRelativeToObject(owner.Transform.position, GameObject.FindObjectOfType<Camera>().transform);
        owner.Transform.position = new Vector3(r.x * rollDistance, r.y, r.z);*/

        owner.StartCoroutine(WaitForRoll(owner, .5f));
    }

    IEnumerator WaitForRoll(Entity owner, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        owner.GetComponent<Actions>().isBusy = false;
        if (owner.GetComponent<Movement>())
            owner.GetComponent<Movement>().AllowMovement = true;
    }
}

/// <summary>
/// Melee hit
/// </summary>
[Serializable]
public class MeleeHit : Action
{
    public MeleeHit(ActionType aType, int requiredStamina)
    {
        base.aType = aType;
        base.requiredStamina = requiredStamina;
    }

    public override void DoAction(Entity owner)
    {
        //play melee hit animation
        owner.GetComponent<Player>().Anim.SetTrigger("Attack1");
        owner.GetComponentInChildren<WeaponTrigger>().DoHit();
        owner.StartCoroutine(WaitForAction(owner, 0.8f));
        owner.GetComponent<Movement>().AllowMovement = false;
    }

    IEnumerator WaitForAction(Entity owner, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        owner.GetComponentInChildren<WeaponTrigger>().ClearHitList();
        owner.GetComponent<Movement>().AllowMovement = true;

        //delay focus regen
        yield return new WaitForSeconds(.2f);
        owner.GetComponent<Actions>().isBusy = false;
        //combo window??
    }
}

/// <summary>
/// Charge hit
/// </summary>
[Serializable]
public class ChargeHit : Action
{
    public ChargeHit(ActionType aType, int requiredStamina)
    {
        base.aType = aType;
        base.requiredStamina = requiredStamina;
    }

    public override void DoAction(Entity owner)
    {
        //play melee hit animation
        owner.GetComponent<Player>().Anim.SetTrigger("Attack2");
        owner.StartCoroutine(WaitForAction(owner, 1.2f));
    }

    IEnumerator WaitForAction(Entity owner, float waitTime)
    {
        WeaponTrigger wt = owner.GetComponentInChildren<WeaponTrigger>();
        wt.weaponDmg_ChangeThis *= 2.5f;
        owner.GetComponent<Movement>().AllowMovement = false;
        yield return new WaitForSeconds(0.7f);
        wt.DoHit();
        yield return new WaitForSeconds(waitTime);
        owner.GetComponent<Movement>().AllowMovement = true;
        wt.ClearHitList();
        wt.weaponDmg_ChangeThis /= 2.5f;

        //delay focus regen
        yield return new WaitForSeconds(.5f);
        owner.GetComponent<Actions>().isBusy = false;
    }
}

/// <summary>
/// Hold shield
/// </summary>
[Serializable]
public class HoldShield : Action
{
    public HoldShield(ActionType aType, int requiredStamina)
    {
        base.aType = aType;
        base.requiredStamina = requiredStamina;
    }

    public override void DoAction(Entity owner)
    {
        //while holding right mouse button, hold up shield
    }
}

/// <summary>
/// Parry
/// </summary>
[Serializable]
public class Parry : Action
{
    public Parry(ActionType aType, int requiredStamina)
    {
        base.aType = aType;
        base.requiredStamina = requiredStamina;
    }

    public override void DoAction(Entity owner)
    {
        //play parry anim and check if parried anything
        owner.StartCoroutine(WaitForAction(owner, 1));
    }

    IEnumerator WaitForAction(Entity owner, int waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        owner.GetComponent<Actions>().isBusy = false;
    }
}

/// <summary>
/// Enemy Melee hit
/// </summary>
[Serializable]
public class EnemyMeleeHit : Action
{
    public EnemyMeleeHit(ActionType aType, int requiredStamina)
    {
        base.aType = aType;
        base.requiredStamina = requiredStamina;
    }

    public override void DoAction(Entity owner)
    {
        //play melee hit animation
        //owner.GetComponent<Player>().Anim.SetTrigger("Attack1");
        owner.StartCoroutine(WaitForAction(owner, 1f));
        owner.GetComponent<NavMeshAgent>().isStopped = true;
    }

    IEnumerator WaitForAction(Entity owner, float waitTime)
    {
        WeaponTrigger wt = owner.GetComponentInChildren<WeaponTrigger>();
        wt.GetComponent<MeshRenderer>().enabled = true;
        yield return new WaitForSeconds(0.7f);
        wt.DoHit();
        yield return new WaitForSeconds(waitTime);
        wt.ClearHitList();
        owner.GetComponent<NavMeshAgent>().isStopped = false;
        wt.GetComponent<MeshRenderer>().enabled = false;

        //delay focus regen
        yield return new WaitForSeconds(.2f);
        owner.GetComponent<Actions>().isBusy = false;
        //combo window??
    }
}
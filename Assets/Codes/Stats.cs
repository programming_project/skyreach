﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Threading;

[RequireComponent(typeof(Entity))]
[RequireComponent(typeof(Abilities))]
public class Stats : MonoBehaviour
{
    public Entity Owner;

    public float health;
    public float maxHealth;

    public float stamina;
    public float maxStamina;
    public float staminaRegenAmount;

    public float Damage;

    public bool canTakeDmg;

   /* public bool canTakeDmg = true;

    private float m_tDmgTimer;
    [SerializeField]
    private float m_tDmgMaxTimer;*/

    public List<DamageType> Damages = new List<DamageType>();
    
    public List<DamageType> Resistances = new List<DamageType>();
    
    private void Awake()
    {
        Owner = GetComponent<Entity>();
    }

    /*private void Update()
    {
        if (m_tDmgTimer <= 0)
        {
            m_tDmgTimer = 0;
            canTakeDmg = true;
        }
        else
            m_tDmgTimer -= Time.deltaTime;
    }*/

    public void TakeDamage(List<DamageTypes> damageType, float damageAmount)
    {
        if (damageAmount <= 0 || !canTakeDmg)
            return;
        float damageToTake = 0;

        // canTakeDmg = false;
        //m_tDmgTimer = m_tDmgMaxTimer;
       
        if (Owner.entityType == EntityType.Enemy)
            ((EnemyAIBase)Owner).FlashHealthBar();

        for (int i = 0; i < damageType.Count(); i++)
        {
            for (int j = 0; j <= Resistances.Count(); j++)
            {
                if (i < Resistances.Count() && damageType[i] == Resistances[j].damageTypes) // Tests if damageType and resistance match, then test if theres any damage left after resistance
                {
                    damageToTake += (damageAmount - Resistances[j].amount > 0) ? damageAmount - Resistances[j].amount : 0;
                    break;
                }
                else if (i == Resistances.Count()) // If none of the resistances match, deal damageAmount of damage
                    damageToTake += damageAmount;
            }
        }
        if (damageToTake <= 0)
            return;
        health -= damageToTake;
        if (health <= 0)
            Died(); 
    }
    
    /// <summary>
    /// Instant heal
    /// </summary>
    /// <param name="amount"></param>
    public void Heal(int amount)
    {
        if (amount <= 0)
            return;
        health += amount;
    }

    /// <summary>
    /// Heal over time
    /// </summary>
    /// <param name="amount">whole amount of the heal</param>
    /// <param name="durationSeconds">duration of the heal in seconds</param>
    /// <param name="TickMs">Tick time in milliseconds</param>
    public void Heal(float amount, float durationSeconds, int TickMs)
    {
        if (amount <= 0 && health > 0)
            return;

        float healPerSecond = amount / durationSeconds;
        TickMs = 1000;
                 
        Thread t = new Thread(() =>
        {
            int healedAmount = 0;
            while(healedAmount == amount && health > 0)
            {
                health += healPerSecond;
                healedAmount++;
                Thread.Sleep(TickMs);
            }
        });
        t.Start();
    }

    public void Died()
    {
        print(Owner.Name + " died lel");
        //here drop loots, display animations etc
        //then disable the object
        if(Owner.entityType == EntityType.Enemy)
            GameManager.GM.player.RemoveTarget(Owner);
        Destroy(gameObject);

        //check if player dies and end the game here

        //Owner.IsEnabled = false;
        //this.enabled = false;
    }
}

//[System.Serializable]
//public struct DamageType
//{
//    public DamageTypes damageTypes;
//    public int amount;
//}

//public enum DamageTypes
//{
//    pierce,
//    cut,
//    slash,
//    blunt,
//    fire,
//    frost,
//    lightning,
//    shadow,
//    light,
//    stun,
//    knockback,
//    poison
//}

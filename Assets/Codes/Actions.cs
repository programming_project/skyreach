﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(Entity))]
public class Actions : MonoBehaviour
{
    public Entity Owner;
    public Stats stats;
    public bool isBusy;

    [SerializeField]
    private Action[] actionHolderArray;

    public Dictionary<ActionType, Action> CurrentActions = new Dictionary<ActionType, Action>();

    private void Awake()
    {
        Owner = GetComponent<Entity>();
        stats = GetComponent<Stats>();

        if(GetComponent<Player>())
        {
            //Add actions here, this is a workaround so you can add actions in unity inspector.
            //Dictionaries cant be viewed in inspector for obvious reasons.
            foreach (Action a in actionHolderArray)
            {
                switch (a.aType)
                {
                    case ActionType.Roll:
                        CurrentActions.Add(a.aType, new Roll(a.aType, a.requiredStamina));
                        break;
                    case ActionType.MainAttack1:
                        CurrentActions.Add(a.aType, new MeleeHit(a.aType, a.requiredStamina));
                        break;
                    case ActionType.MainAttack2:
                        CurrentActions.Add(a.aType, new ChargeHit(a.aType, a.requiredStamina));
                        break;
                    default:
                        CurrentActions.Add(a.aType, a);
                        break;
                }

            }
        }
        //enemy actions
        else
        {
            foreach (Action a in actionHolderArray)
            {
                switch (a.aType)
                {
                    case ActionType.MainAttack1:
                        CurrentActions.Add(a.aType, new EnemyMeleeHit(a.aType, a.requiredStamina));
                        break;
                    default:
                        CurrentActions.Add(a.aType, a);
                        break;
                }

            }
        }     
    }

    //TODO: Move this to the entity class so all updates are in one class
    private void Update()
    {
        if(Owner.IsEnabled)
            RegenStamina();
    }

    public void RegenStamina()
    {
        if (isBusy)
            return;
        if (stats.stamina < stats.maxStamina)
            stats.stamina += ((1 + stats.staminaRegenAmount) * Time.deltaTime);
        if (stats.stamina > stats.maxStamina)
            stats.stamina = stats.maxStamina;
    }

    public bool DoAction(Action action)
    {
        //If we are already doing something or we dont have enough stats.Stamina
        if (isBusy || (stats.stamina - action.requiredStamina) < 0)
        {
            action = null;
            return false;
        }

        isBusy = true;
        stats.stamina -= action.requiredStamina;
        action.DoAction(Owner);
        return true;
    }
}

[Serializable]
public class Action
{
    public ActionType aType;
    public int requiredStamina;

    /// <summary>
    /// Override this and do the action here.
    /// </summary>
    /// <param name="Owner"></param>
    public virtual void DoAction(Entity Owner)
    {

    }
}

public enum ActionType
{
    MainAttack1,
    MainAttack2,
    Parry,
    Block,
    Roll
}

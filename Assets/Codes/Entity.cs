﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Transform))]
[RequireComponent(typeof(Stats))]
[RequireComponent(typeof(Inventory))]
public class Entity : MonoBehaviour
{
    public string Name { get; set; }
    public bool IsEnabled { get; set; }
    public Transform Transform { get; set; }

    [HideInInspector]
    public EntityType entityType;

    public Inventory inventory;

    public Stats stats;

    protected virtual void Awake()
    {
        Transform = GetComponent<Transform>();
        IsEnabled = true;
    }

    /// <summary>
    /// Call this to update entity
    /// </summary>
    protected virtual void Update()
    {
        if (!IsEnabled)
            return;
    }
}

public enum EntityType
{
    Player,
    Enemy
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBox : MonoBehaviour
{
    private Player m_player;

    private void Awake()
    {
        m_player = transform.root.GetComponent<Player>();
    }
    private void OnTriggerEnter(Collider col)
    {
        Entity e = col.GetComponent<Entity>();
        if (e != null)
            m_player.AddTarget(e);
    }
    private void OnTriggerExit(Collider col)
    {
        Entity e = col.GetComponent<Entity>();
        if (e != null)
            m_player.RemoveTarget(e);
    }
}

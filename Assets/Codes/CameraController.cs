﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	
	private Transform m_transform;
	public Transform playerTransform;

    private Rigidbody m_rig;
    private Player m_player;

    public Rigidbody Rig { get { return m_rig; } set { m_rig = value; } }
    public Player Player { get { return m_player; } set { m_player = value; } }

    private float m_x;
	private float m_y;

    public float minY, maxY;

    public float xSpeed;
	public float ySpeed;
	
	private Vector3 m_angles;
	
	public float distanceMin, distanceMax;
	
	public float distance = 0;
	
	private Vector3 velocity;
	
	public float followSpeed;

  

    public LayerMask mask;
	
	private void Awake()
	{
		m_transform = transform;
        m_angles = m_transform.eulerAngles;
		m_x = m_angles.y;
		m_y = m_angles.x;
	}

    private void FixedUpdate ()
	{
        Vector3 desiredPos;

        //if we are locked to target
        if (m_player.Locked && m_player.CurrentTarget != null)
        {
            Vector3 targetPostition = new Vector3(m_player.CurrentTarget.transform.position.x, m_transform.position.y - 5, m_player.CurrentTarget.transform.position.z);
            m_transform.LookAt(targetPostition);
            desiredPos = m_transform.rotation * new Vector3(0, 0, -distance) + playerTransform.position;
        }
        else
        {
            m_x += Input.GetAxis("Mouse X") * xSpeed;
            m_y -= Input.GetAxis("Mouse Y") * ySpeed;

            Quaternion rotation = Quaternion.Euler(m_y, m_x, 0);

            m_transform.rotation = rotation;

            desiredPos = rotation * new Vector3(0, 0, -distance) + playerTransform.position;
        }

        m_y = ClampAngle(m_y, minY, maxY);

      
        // Smooth y axis to slowly follow player.
        float smoothY = Mathf.SmoothDamp(m_transform.position.y + .035f, desiredPos.y, ref velocity.y, followSpeed);
        m_transform.position = new Vector3(desiredPos.x, smoothY, desiredPos.z);

        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

        RaycastHit hit;
        if (Physics.Raycast(playerTransform.position, (m_transform.position - playerTransform.position).normalized, out hit, (distance <= 0 ? -distance : distance), mask))
        {
            m_transform.position = hit.point - (m_transform.position - hit.point).normalized * 1.2f;
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;

        return Mathf.Clamp(angle, min, max);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class SkeletonAI : EnemyAIBase
{
    private Animator m_anim;

	private void Start ()
    {
        m_anim = transform.GetComponentInChildren<Animator>();
    }

    protected override void Update()
    {
        base.Update();
        m_anim.SetFloat("speed", GetComponent<NavMeshAgent>().velocity.magnitude);
    }

    protected override void Attack()
    {
        if (m_attackTimer <= 0)
        {
            bool success = false;
            success = m_actions.DoAction(m_actions.CurrentActions[ActionType.MainAttack1]);
            m_attackTimer = m_attackMaxTimer;

            if(success)
                m_anim.SetTrigger("heavy");
        }
      
    }
	
}

﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Entity))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Stats))]
public class Movement : MonoBehaviour {
	
	public bool AllowMovement;
	public float speed;

    private Rigidbody m_rigidbody;
    private Vector3 m_moveVector = Vector3.zero;
    private Vector3 m_xMoveVector = Vector3.zero;
	private Transform m_transform;
	private bool m_isGrounded;
    private bool m_doubleJumpUsed = false;
	private float m_yVelocity;

    [SerializeField]
    private Camera m_playerCam;

    public Camera PlayerCam
    {
        get {return m_playerCam;}
        set {m_playerCam = value;}
    }
	
	[SerializeField]
	private float m_fallingSpeed;
	
	[SerializeField]
	private float m_jumpSpeed;
	
	[SerializeField]
	private LayerMask m_layerMask;
	
	public float turnSpeed;

    private Entity owner;
    private Player m_player;
    private Stats stats;
	
	void Awake()
	{
		m_transform = transform;
		m_rigidbody = GetComponent<Rigidbody>();
        owner = GetComponent<Entity>();
        stats = GetComponent<Stats>();
        if(owner.GetType() == typeof(Player))
            m_player = (Player)owner;
    }
	
	void FixedUpdate()
	{
		if(AllowMovement && owner.IsEnabled && owner.entityType == EntityType.Player)
        {
            //CheckForInput();
            DoMovement();
        }
        
        //climb wall
        if(Physics.Raycast(transform.position,(transform.TransformDirection(Vector3.forward)) ,0.75f) && Input.GetKey(KeyCode.Space))
        {
            m_yVelocity = m_jumpSpeed;
        }
	}

    /// <summary>
    /// Do player movement
    /// </summary>
    void DoMovement()
	{
        m_rigidbody.velocity = Vector3.zero;
        m_moveVector = m_xMoveVector;
		
        //ollaanko maassa
		RaycastHit ray;

        if (Physics.SphereCast(m_transform.position, .5f, Vector3.down, out ray, .6f, m_layerMask))
        {
            m_isGrounded = true;
            m_doubleJumpUsed = false;
        }
        else
            m_isGrounded = false;
		
        //hyppy
		if(Input.GetKeyDown(KeyCode.Space) && m_isGrounded)
		{
			m_yVelocity = m_jumpSpeed;
            m_player.Anim.SetTrigger("Jump");
		}
        if (Input.GetKeyDown(KeyCode.Space) && !m_isGrounded && !m_doubleJumpUsed)
        {
            m_yVelocity = m_jumpSpeed;
            m_doubleJumpUsed = true;
            m_player.Anim.SetTrigger("Jump");
        }
        if (m_isGrounded && m_yVelocity < 0)
		{
			m_yVelocity = -0.1f;
		}
		else
		{
			m_yVelocity += Physics.gravity.y * m_fallingSpeed * Time.deltaTime;
		}
		
		m_moveVector.y = m_yVelocity * Time.deltaTime;

        m_xMoveVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //lock to target
        if (m_player.Locked && m_player.CurrentTarget != null)
        {
            m_xMoveVector = m_transform.TransformDirection(m_xMoveVector);
            m_transform.LookAt(new Vector3(m_player.CurrentTarget.transform.position.x, m_transform.position.y, m_player.CurrentTarget.transform.position.z));
        }
        else
        {
            m_xMoveVector = GetVectorRelativeToObject(m_xMoveVector, m_playerCam.transform);

            if (m_xMoveVector.magnitude > 0)
            {
                Quaternion targetRotation = Quaternion.LookRotation(m_xMoveVector, Vector3.up);
                m_transform.rotation = Quaternion.Slerp(m_transform.rotation, targetRotation, 0.2f);
            }
        }
      
        m_xMoveVector *= speed;
        m_player.Anim.SetFloat("Speed", m_xMoveVector.magnitude);

        m_rigidbody.velocity = m_moveVector;
	}


    public static Vector3 GetVectorRelativeToObject(Vector3 inputVector, Transform camera)
    {
        Vector3 objectRelativeVector = Vector3.zero;
        if (inputVector != Vector3.zero)
        {
            Vector3 forward = camera.TransformDirection(Vector3.forward);
            forward.y = 0f;
            forward.Normalize();
            Vector3 right = new Vector3(forward.z, 0.0f, -forward.x);

            Vector3 relativeRight = inputVector.x * right;
            Vector3 relativeForward = inputVector.z * forward;

            objectRelativeVector = relativeRight + relativeForward;

            if (objectRelativeVector.magnitude > 1f) objectRelativeVector.Normalize();
        }
        return objectRelativeVector;
    }

    //https://github.com/jm991/UnityThirdPersonTutorial/tree/master/Assets/Scripts

    /*[SerializeField]
    private ActionKey[] Keybinds;

    private void CheckForInput()
    {
        foreach(ActionKey ac in Keybinds)
        {
            if(Input.GetKeyDown(ac.kc))
            {
                actions.DoAction(actions.CurrentActions[ac.at]);
                return;
            }
        }
    }

    [Serializable]
    public struct ActionKey
    {
        public KeyCode kc;
        public ActionType at;
    }*/
}
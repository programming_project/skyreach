﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff
{
    [SerializeField]
    protected float m_buffDuration;

    public float curDuration;

    public bool isActive;

    public bool isPassive;

    public Buff()
    {

    }

    public virtual void StartBuffTimer()
    {
        curDuration = m_buffDuration;
        isActive = true;
        //do other stuff
    }

    public virtual void RemoveBuff()
    {
        isActive = false;
        //end the buff
    }
    
    public virtual void DoUpdate()
    {
        if (isActive && curDuration > 0)
            curDuration -= Time.deltaTime;
        if (isActive && curDuration <= 0)
        {
            RemoveBuff();
        }
    }
}

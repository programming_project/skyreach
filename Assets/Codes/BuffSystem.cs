﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Entity))]
public class BuffSystem : MonoBehaviour
{
    public List<Buff> buffList = new List<Buff>();

	void Update ()
    {
        for (int i = 0; i < buffList.Count; i++)
            buffList[i].DoUpdate();
	}
}

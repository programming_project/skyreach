﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemBase
{
    public string iName;
    public int iId;
    public ItemType iType;
    public Sprite iSprite;
    public GameObject iModel;
    public string iDesc;
    
}

public enum ItemType
{
    Weapon,
    Armour,
    Consumable,
    Rune
}

[System.Serializable]
public class Weapon : ItemBase
{
    public WeaponTypes weaponType;
    public DamageType baseDamage;
    public DamageType extraAbility;

    public Weapon(string name, WeaponTypes weaponType, DamageTypes baseDamage, int damageAmount)
    {
        this.iName = name;
        this.weaponType = weaponType;
        this.baseDamage.damageTypes = baseDamage;
        this.baseDamage.amount = damageAmount;
    }
    
}

public enum WeaponTypes
{
    Sword,
    Axe,
    Hammer,
    GreatSword,
    BattleAxe,
    Maul,
    Dagger,
    Shield
}

[System.Serializable]
public class Armour : ItemBase
{
    public ArmourTypes armourType;
    public DamageType baseResistance;
    public DamageType extraAbility;

    public Armour(string name, ArmourTypes armourType, DamageTypes baseDamage, int resistaceAmount)
    {
        this.iName = name;
        this.armourType = armourType;
        this.baseResistance.damageTypes = baseDamage;
        this.baseResistance.amount = resistaceAmount;
    }
}

public enum ArmourTypes
{
    Helmet,
    Cuirass,
    Boots
}

[System.Serializable]
public class Consumable : ItemBase
{


}

[System.Serializable]
public class Rune : ItemBase
{


}
    
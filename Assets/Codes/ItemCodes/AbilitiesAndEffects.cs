﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Stats))]
public class Abilities : MonoBehaviour {
    
    /* LIST OF ABILITIES AND EFFECTS
    BASE
    pierce resistance and damage
    cut resistance and damage
    hack resistance and damage
    blunt resistance and damage
    EXTRA
    fire resistance and damage
    frost resistance and damage
    lightning resistance and damage
    shadow resistance and damage
    light resistance and damage
    stun resistance and damage
    knockback resistance and damage
    poison resistance and damage
    
    extra armor
    extra health
    extra stamina
    extra healing received
    extra stamina regen

    lifesteal
    staminasteal
    
    */

    void GetAbility()
    {
        //something
    }






}

public class BaseAbility
{
    public DamageType baseAbility;

    public virtual void DoAbility()
    {
        
        
    }
}

public class LifeSteal : BaseAbility
{
    public override void DoAbility()
    {
        base.DoAbility();

        // what this does...
    }
}

[System.Serializable]
public struct DamageType
{
    public DamageTypes damageTypes;
    public int amount;
}

public enum DamageTypes
{
    pierce,
    cut,
    slash,
    blunt,
    fire,
    frost,
    lightning,
    shadow,
    light,
    stun,
    knockback,
    poison
}

//public enum WeaponBaseAbilities {
//    pierceDamage,
//    cutDamage,
//    hackDamage,
//    bluntDamage
//}

//public struct WeaponBaseAbility
//{
//    public WeaponBaseAbilities ability;
//    int amount;
//}

//public enum WeaponExtraAbilities
//{
//    fireDamage,
//    frostDamage,
//    lightningDamage,
//    shadowDamage,
//    lightDamage,
//    stunDamage,
//    knockbackDamage,
//    poisonDamage,
//    lifeSteal,
//    staminaSteal
//}

//public struct WeaponExtraAbility
//{
//    public WeaponExtraAbilities ability;
//    int amount;
//}

//public enum ArmourBaseAbilities
//{
//    pierceResistance,
//    cutResistance,
//    hackResistance,
//    bluntResistance
//}

//public struct ArmourBaseAbility
//{
//    public ArmourBaseAbilities ability;
//    int amount;
//}

//public enum ArmourExtraAbilities
//{
//    fireResistance,
//    frostResistance,
//    lightningResistance,
//    shadowResistance,
//    lightResistance,
//    stunResistance,
//    knockbackResistance,
//    poisonResistance,
//    extraArmor
//}

//public struct ArmourExtraAbility
//{
//    public ArmourExtraAbilities ability;
//    int amount;
//}

//public enum CommonAbilities
//{
//    extraHealth,
//    extraStamina,
//    extraHealingReceived,
//    extraStaminaRegen
//}

//public struct CommonAbility
//{
//    public CommonAbilities ability;
//    int amount;
//}


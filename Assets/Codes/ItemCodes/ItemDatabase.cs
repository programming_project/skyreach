﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    [SerializeField]
    private List<ItemBase> itemList = new List<ItemBase>();

    public void InitItems()
    {
        Weapon s = new Weapon("Sword", WeaponTypes.Sword, DamageTypes.cut, 10);
        AddItem(s);
        Armour c = new Armour("Cuirass", ArmourTypes.Cuirass, DamageTypes.cut, 10);
        AddItem(c);
    }

    /// <summary>
    /// Returns item from database
    /// </summary>
    /// <param name = "id">Id of wanted item</param>
    // <param name="itemList">List of items from ItemDatabase</param>
	public /*static*/ ItemBase GetItem(int id/*, List<ItemBase> itemList*/)
    {
        ItemBase temp = null;

        for (int i = 0; i < itemList.Count; i++)
        {
            if(itemList[i].iId == id)
                temp = itemList[id];
        }

        if (temp != null)
            return temp;
        else
            return null;
    }

    public void AddItem(ItemBase item)
    {
        item.iId = itemList.Count;
        itemList.Add(item);
    }
}

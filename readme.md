﻿Peli
----
<ul>
    <li>3d-platformer (jumping, wall climbing, wallrun etc.)</li>
    <li>solid battle mechanics (Soulslike fast paced)</li>
    <li>Theme (Feudal asian fantasy surroundings)</li>
    <li>Story based (to do)</li>
    <li>World consists of hub-areas which lead to multiple sub-levels.</li>
    <li>Gear alters character stats. (No leveling)</li>
    <li>Chests, item drops. (not random)</li>
    <li>Flying creatures. (possibly flying combat????)</li>
    <li>Boss enemies, also legendary enemies with legendary loot.</li>
    <li>Rune-"library", weapon enchantments can be transferred to runes and you can enchant your own weapon with your favourite enchantment. (enchantments are like life-steal or burning etc.)</li>
    <li>You can upgrade normal weapons with the corresponding material.(special weapons can not be modified at all)</li>
    <li>Basic systems(UI, saving, controller support... "kyllä töitä riittää" -Ati)</li>
</ul>

![](diagram.PNG)